# About

These are my dotfiles and they are managed by
[Dotbot](https://github.com/anishathalye/dotbot.git), a submodule in this
repository. Files will be symlinked into the relevant directories using the
install script. The Configuration for the install script is maintained by
`install.conf.yaml`.


# Installation

- `./install`
- The only thing that may need to be done is to build the vim-coc plugin by running
`yarn install`.
- Yarn can be installed with a package manager or `conda`
