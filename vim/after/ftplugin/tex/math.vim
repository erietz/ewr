
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" LaTeX Math mode
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
inoremap <leader>/ \frac{}{<++>}<Esc>F}i
inoremap `a \alpha
inoremap `b \beta
inoremap `g \gamma
inoremap `d \delta
inoremap `t \theta
inoremap `l \lambda
inoremap `p \phi
inoremap `s \sigma

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
